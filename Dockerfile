FROM python:3.6.3

LABEL maintainers="Kopachev Vitaly <kopachyov.vitaliy@yandex.ru>"

# Settings application environment
COPY . /app
WORKDIR /app

RUN pip install --upgrade pip
RUN pip install -r requirements.txt

ENTRYPOINT ["python", "manage.py"]
