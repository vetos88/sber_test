"""
    Конфигурации бекенда
"""

import os

# Режим дебага
DEBUG_MODE = os.environ.get('DEBUG_MODE', 'TRUE') == 'TRUE'

# Порт приложения
WEB_APP_PORT = os.environ.get('PORT', 8080)

ADMINISTRATOR_USER_NAME = os.environ.get('ADMINISTRATOR_USER_NAME', 'Администратор')
