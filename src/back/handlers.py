"""
    Обработчики
"""
import uuid

from aiohttp import web, WSMsgType
import aiohttp_jinja2

from src.back.config import ADMINISTRATOR_USER_NAME


class ClientApplicationView(web.View):
    """
        Предсталвеление клиентского приложения
    """
    @aiohttp_jinja2.template('index.html')
    async def get(self):
        """
            На запрос выдается страница с бандлом клиентского js приложения.
        """
        return {}


class WebSocket(web.View):
    """
        Основная бизнес логика чата.
        Сообщения от пользователей отправляются только админу.
    """
    async def check_socket_and_send_message(self, _ws, user_name, message):
        """
            Перехват ошибки при отправки через сокет
        """
        try:
            await _ws.send_str(message)
        except (ConnectionResetError, RuntimeError):
            print('Cant send message {} to {}'.format(message, user_name))
            print('Sockets count {}'.format(len(self.request.app['websockets'])))

    async def get(self):
        ws = web.WebSocketResponse()
        await ws.prepare(self.request)
        user_name = self.request.query.get('user')
        user_guid = str(uuid.uuid4())
        for user_ws in self.request.app['websockets']:
            if user_ws.get('user_name') == ADMINISTRATOR_USER_NAME:
                _ws = user_ws.get('socket')
                await self.check_socket_and_send_message(_ws, user_name, '{}: я подключился!'.format(user_name))
        self.request.app['websockets'].append({
            'guid': user_guid,
            'user_name': user_name,
            'socket': ws
        })
        print('{} websocket connection open'.format(user_name))
        async for msg in ws:
            print('Got message from {}'.format(user_name))
            if msg.type == WSMsgType.TEXT:
                if msg.data == 'close':
                    await ws.close()
                else:
                    if user_name == ADMINISTRATOR_USER_NAME:
                        received_payload = msg.data.split(': ')
                        # Ожидается строка в виде user_name: message,
                        # если нет то сообщение отправляется всем.
                        if len(received_payload) > 1:
                            for user_ws in self.request.app['websockets']:
                                _ws = user_ws.get('socket')
                                if user_ws.get('user_name') == received_payload[0]:
                                    await self.check_socket_and_send_message(
                                        _ws, user_name,
                                        '{}: {}'.format(user_name, ': '.join(received_payload[1:]))
                                    )
                                elif (user_ws.get('user_name') == ADMINISTRATOR_USER_NAME
                                        and user_ws['guid'] != user_guid):
                                    await self.check_socket_and_send_message(
                                        _ws, user_name,
                                        '{}: {}'.format(user_name, msg.data)
                                    )
                        else:
                            for user_ws in self.request.app['websockets']:
                                if user_ws['guid'] != user_guid:
                                    _ws = user_ws.get('socket')
                                    await self.check_socket_and_send_message(
                                        _ws, user_name,
                                        '{}: {}'.format(user_name, received_payload[0])
                                    )
                    else:
                        for user_ws in self.request.app['websockets']:
                            if user_ws.get('user_name') == ADMINISTRATOR_USER_NAME:
                                _ws = user_ws.get('socket')
                                await self.check_socket_and_send_message(
                                    _ws, user_name,
                                    '{}: {}'.format(user_name, msg.data)
                                )
            elif msg.type == WSMsgType.ERROR:
                print('ws connection closed with exception %s' %
                      ws.exception())

        self.request.app['websockets'] = [
            user_ws for user_ws in self.request.app['websockets'] if user_ws['guid'] != user_guid
        ]
        for user_ws in self.request.app['websockets']:
            if user_ws.get('user_name') == ADMINISTRATOR_USER_NAME:
                _ws = user_ws.get('socket')
                await self.check_socket_and_send_message(
                    _ws, user_name,
                    '{}: отключился!'.format(user_name)
                )
        print('{} websocket connection closed'.format(user_name))

        return ws
