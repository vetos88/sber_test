"""
    Бекенд
"""
import asyncio
import os

import aiohttp_jinja2
import jinja2
import uvloop

from aiohttp import web

from src.back.config import WEB_APP_PORT, DEBUG_MODE
from src.back.handlers import WebSocket, ClientApplicationView

# Рабочая папка
WORK_DIR = os.getcwd()

# URI по которым выжается шаблоны.
UI_ROUTE_URLS = [
    ('GET', r'/', ClientApplicationView, {'name': 'main_app_bundle'}),
]

# api URI
API_ROUTE_URLS = [
    ('GET', '/ws/chat', WebSocket, {'name': 'chat'}),
]

ROUTE_URLS = UI_ROUTE_URLS + API_ROUTE_URLS

# Реестр вебсокетов
# {'user_name': 'user', 'socket': ws}
ACTIVE_WEB_SOCKETS = []


async def on_shutdown(app):
    """
        Закрытие всех вебсокетов
    """
    for user_ws in app['websockets']:
        await user_ws.get('socket').close(code=1001, message='Server shutdown')


def init(loop):
    """
        Инициализация веб приложения.
    """
    app = web.Application(loop=loop, middlewares=[], debug=DEBUG_MODE)
    app['websockets'] = ACTIVE_WEB_SOCKETS
    # handler = app.make_handler()

    aiohttp_jinja2.setup(
        app,
        loader=jinja2.FileSystemLoader(os.path.join(WORK_DIR, 'src','front'), followlinks=True)
    )
    app.router.add_static('/static/', path=os.path.join(WORK_DIR, 'src', 'front', 'static'))
    for method, reg, handler, kwargs in ROUTE_URLS:
        app.router.add_route(method, reg, handler, name=kwargs['name'])
        app.router.add_route(method, reg + "/", handler, name=(kwargs['name'] + '_slash'))

    app.on_cleanup.append(on_shutdown)
    return app


def main():
    """
        Запуск приложения
    """
    asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
    loop = asyncio.get_event_loop()
    app = init(loop)
    web.run_app(app, port=WEB_APP_PORT)
    loop.close()
